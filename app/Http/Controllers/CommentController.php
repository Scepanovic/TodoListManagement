<?php namespace App\Http\Controllers;

use Illuminate\Http\Response;
use Illuminate\Http\Request;
use App\Models\Comment;

class CommentController extends Controller {

	public function index()
	{
		return response()->json(Comment::all());
		// return "Komentar";
	}

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
    	$comm = new Comment;
    	$comm->text = $request->input('text');
    	$comm->marked = 0;
    	$comm->priority = 0;
		
		$comm->save();

    	return response()->json($comm);
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
    	// Comment::destroy($id);
    	$comm = Comment::find($id);

		$comm->delete();

    	return response()->json(array('success' => true));
    }

    public function update(Request $request, $id){

    	$comm = Comment::find($id);

    	if($request->has('marked'))
    	{
    		$comm->marked = $request->input('marked');
    	}

    	else if ($request->has('text')) 
    	{
    		$comm->text = $request->input('text');
    	}

    	else if ($request->has('priority')){
    		$comm->priority = $request->input('priority');
    	}
    	
		$comm->save();
    }

}
