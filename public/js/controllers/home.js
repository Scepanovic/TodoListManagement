var starterControllers = angular.module('starterControllers', []);

starterControllers.controller('HomeCtrl', function ($scope, $http, $window, Comment) {
  $scope.message = "Angular say hello!";
  $scope.allComments = "";
  
  Comment.get()
    	.success(function(data) {
            $scope.allComments = data;
            console.log($scope.allComments);
         });

  $scope.add = function(text){
  	Comment.save(text).success(function(data){
  		// console.log(data);
  		$scope.allComments.push(data);
  		console.log($scope.allComments);
  	});
  	 
  }

  $scope.mark = function(id){
  	Comment.mark(id);
  	change('mark', id);
  }

  $scope.remove = function(id){
  	Comment.destroy(id);
  	change('remove', id);
  }

  $scope.update = function(id, text){
  	Comment.update(id, text);
  	change('unmark', id, text);
  }

  $scope.unmark = function(id){
  	Comment.unmark(id);
  	change('unmark', id);
  }

  $scope.priorityUp = function(id){
  	Comment.priorityUp(id);
  	change('priorityUp', id);
  }

   $scope.priorityDown = function(id){
  	Comment.priorityDown(id);
  	change('priorityDown', id);
  }


  function change(action, id, text){
  	for(var i = 0 ; i < $scope.allComments.length; i++){
  		if($scope.allComments[i].id == id){
  			if(action == 'remove')
  				$scope.allComments.splice(i,1);

  			if(action == 'priorityUp')
  				$scope.allComments[i].priority = 1;

  			if(action == 'priorityDown')
  				$scope.allComments[i].priority = 0;

  			if(action == 'mark')
  				$scope.allComments[i].marked = 1;

  			if(action == 'unmark')
  				$scope.allComments[i].marked = 0;

  			if(action == 'update')
  				$scope.allComments[i].text = 0;

  		}
  	}
  }

});

