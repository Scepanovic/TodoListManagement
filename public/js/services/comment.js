var starterServices = angular.module('starterServices', []);

starterServices.factory('Comment', function($http) {

    return {
        // get all the comments
        get : function() {
            return $http.get('api/comments');
        },

        // save a comment (pass in comment data)
        save : function(commentData) {
            return $http.post('api/comments', {'text': commentData});
        },

        // destroy a comment
        destroy : function(id) {
            return $http.delete('/api/comments/' + id);
        },

        update : function(id, text){

             // return  $http.post('comments/update', {'id': id, 'text': text});
             // return  $http.post('comments/update',  headers: {'Content-Type': "Origin, X-Requested-With, Content-Type, Accept"}, {'id': id, 'text': text});
             return  $http.put('api/comments/' + id, {'id': id, 'text': text});
        },

        mark : function(id){

             return  $http.put('api/comments/' + id, {'marked': 1});
        },

        unmark : function(id){
            
             return  $http.put('api/comments/' + id, {'marked': 0 });
        },

        priorityUp : function(id){
            return  $http.put('api/comments/' + id, {'priority': 1});
        },

        priorityDown : function(id){
            return  $http.put('api/comments/' + id, {'priority': 0});
        }
    }

});
