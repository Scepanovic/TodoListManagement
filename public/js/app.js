var starter = angular.module('starter', ['ngRoute','starterControllers','starterServices'
], function($interpolateProvider) {
	$interpolateProvider.startSymbol('<%');
	$interpolateProvider.endSymbol('%>');
});

starter.config(['$routeProvider',
	function($routeProvider) {
		$routeProvider.
		when('/test', {
			templateUrl: 'partials/test.html',
			controller: 'HomeCtrl'
		}).
		otherwise({
			redirectTo: '/test'
	});
}]);